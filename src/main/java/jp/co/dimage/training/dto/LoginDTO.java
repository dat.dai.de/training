package jp.co.dimage.training.dto;

import jp.co.dimage.training.utils.ValidationGroupPassword;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class LoginDTO {
    @NotNull(message = "username.is.empty", groups = {ValidationGroupPassword.class})
    @Size(max = 45, min = 1, message = "username.over.length", groups = {ValidationGroupPassword.class})
    private String username;
    @NotNull(message = "password.is.empty", groups = {ValidationGroupPassword.class})
    @Size(max = 45, min = 1, message = "password.over.length", groups = {ValidationGroupPassword.class})
    private String password;
}
