package jp.co.dimage.training.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class ResponseSearchDTO implements Serializable {
    private Object items;
    private long total;
}
