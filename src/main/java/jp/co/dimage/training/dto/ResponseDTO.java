package jp.co.dimage.training.dto;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ResponseDTO {
  private String status;
  private String message;
  private Object data;

  public ResponseDTO() {
  }

  public ResponseDTO(String status, String message, Object data) {
    this.status = status;
    this.message = message;
    this.data = data;
  }
}
