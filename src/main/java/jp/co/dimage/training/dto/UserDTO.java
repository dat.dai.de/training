package jp.co.dimage.training.dto;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class UserDTO extends LoginDTO implements Serializable {
    private Long id;
    @Size(max = 200, message = "fullName.over.length")
    private String fullName;
    @NotNull(message = "email.is.empty")
    @Size(max = 200, message = "email.over.length")
    @Pattern(regexp = "^[a-z][a-z0-9_.]{1,32}@[a-z0-9]{2,}(\\.[a-z0-9]{2,4}){1,2}$", message = "email.not.valid")
    private String email;
    private Date dob;
    @NotNull(message = "isAdmin.is.empty")
    private Short isAdmin;
}
