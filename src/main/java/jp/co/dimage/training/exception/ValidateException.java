package jp.co.dimage.training.exception;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class ValidateException extends Exception {

  public ValidateException(String message) {
    super(message);
  }
}
