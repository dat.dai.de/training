package jp.co.dimage.training.controller;

import jp.co.dimage.training.dto.LoginDTO;
import jp.co.dimage.training.dto.ResponseDTO;
import jp.co.dimage.training.dto.UserDTO;
import jp.co.dimage.training.dto.UserDetailsDTO;
import jp.co.dimage.training.exception.ValidateException;
import jp.co.dimage.training.jwt.JwtTokenProvider;
import jp.co.dimage.training.service.UserService;
import jp.co.dimage.training.utils.Constants;
import jp.co.dimage.training.utils.Translator;
import jp.co.dimage.training.utils.ValidationGroupPassword;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Objects;

@Slf4j
@RestController
@RequestMapping("/api")
public class UserController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenProvider tokenProvider;

    @Autowired
    private UserService userService;

    @PostMapping(value = "/login")
    public ResponseEntity<?> getStringAuth(@RequestBody @Validated(ValidationGroupPassword.class) LoginDTO loginDTO) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginDTO.getUsername(),
                        loginDTO.getPassword()
                )
        );

        UserDetailsDTO userDetailsDTO = (UserDetailsDTO) authentication.getPrincipal();
        String jwt = tokenProvider.generateToken(userDetailsDTO);

        return ResponseEntity.status(HttpStatus.OK)
                .header(HttpHeaders.ACCESS_CONTROL_ALLOW_HEADERS, "*")
                .header(HttpHeaders.ACCESS_CONTROL_EXPOSE_HEADERS, HttpHeaders.AUTHORIZATION)
                .header(HttpHeaders.AUTHORIZATION, jwt)
                .body(new ResponseDTO(HttpStatus.OK.name(), Translator.translate(Constants.SUCCESS), userDetailsDTO.getUserDTO()));
    }

    @Secured("ROLE_ADMIN")
    @PostMapping(value = "/create-user", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> createUser(@RequestBody @Valid UserDTO userDTO) throws ValidateException {
        userService.createUser(userDTO);
        return ResponseEntity.ok(new ResponseDTO(HttpStatus.OK.name(), Translator.translate(Constants.SUCCESS), null));
    }

    @Secured("ROLE_ADMIN")
    @DeleteMapping(value = "/delete-user/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> deleteUser(@PathVariable("id") Long id){
        userService.deleteUser(id);
        return ResponseEntity.ok(new ResponseDTO(HttpStatus.OK.name(), Translator.translate(Constants.SUCCESS), null));
    }

    @PutMapping(value = "/update-user", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> updateUser(@RequestBody @Validated UserDTO userDTO) throws ValidateException {
        UserDetailsDTO userDetailsDTO = (UserDetailsDTO) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(!Objects.equals((short)1, userDetailsDTO.getUserDTO().getIsAdmin()))
            userDTO.setIsAdmin((short)0);
        if(!userService.updateUser(userDTO))
            return ResponseEntity.badRequest().body(new ResponseDTO(HttpStatus.OK.name(), Translator.translate(Constants.SYSTEM_ERROR), null));
        return ResponseEntity.ok(new ResponseDTO(HttpStatus.OK.name(), Translator.translate(Constants.SUCCESS), null));
    }

    @Secured("ROLE_ADMIN")
    @GetMapping(value = "/get-all-user", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getAllUser(Pageable pageable){
        return ResponseEntity.ok(new ResponseDTO(HttpStatus.OK.name(), Translator.translate(Constants.SUCCESS), userService.getAllListUser(pageable)));
    }

    @GetMapping(value = "/get-user", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getUser(){
        UserDetailsDTO userDetailsDTO = (UserDetailsDTO) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return ResponseEntity.ok(new ResponseDTO(HttpStatus.OK.name(), Translator.translate(Constants.SUCCESS), userService.getOne(userDetailsDTO.getUserDTO().getId())));
    }

    @PutMapping(value = "/change-pass", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> updateUser(@RequestBody @Validated(ValidationGroupPassword.class) LoginDTO loginDTO){
        UserDetailsDTO userDetailsDTO = (UserDetailsDTO) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        userService.changePassword(loginDTO.getPassword(), userDetailsDTO.getUserDTO().getId());
        return ResponseEntity.ok(new ResponseDTO(HttpStatus.OK.name(), Translator.translate(Constants.SUCCESS), null));
    }
}
