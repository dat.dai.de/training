package jp.co.dimage.training.service;

import jp.co.dimage.training.domain.User;
import jp.co.dimage.training.dto.ResponseSearchDTO;
import jp.co.dimage.training.dto.UserDTO;
import jp.co.dimage.training.dto.UserDetailsDTO;
import jp.co.dimage.training.exception.ValidateException;
import jp.co.dimage.training.mapper.BaseMapper;
import jp.co.dimage.training.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class UserServiceImpl implements UserService{
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    private BaseMapper<User, UserDTO> mapper = new BaseMapper<>(User.class, UserDTO.class);

    private boolean checkExistUsername(String username){
        return userRepository.findByUsername(username) != null;
    }

    private boolean checkExistEmail(String email){
        return userRepository.findByEmail(email) != null;
    }

    public void createUser(UserDTO userDTO) throws ValidateException {
        userDTO.setId(null);
        userDTO.setPassword(passwordEncoder.encode("1"));
        if (checkExistUsername(userDTO.getUsername()))
            throw new ValidateException("username.is.exist");
        if(checkExistEmail(userDTO.getEmail()))
            throw new ValidateException("email.is.exist");
        User newUser = mapper.toPersistenceBean(userDTO);
        userRepository.saveAndFlush(newUser);
    }

    public boolean updateUser(UserDTO userDTO) throws ValidateException {
        if(userDTO.getId() == null)
            return false;
        User user = userRepository.findById(userDTO.getId()).orElse(null);
        if(user == null)
            return false;
        if(checkExistEmail(userDTO.getEmail()))
            throw new ValidateException("email.is.exist");
        User newUser = mapper.toPersistenceBean(userDTO);
        newUser.setPassword(user.getPassword());
        newUser.setUsername(user.getUsername());
        userRepository.saveAndFlush(newUser);
        return true;
    }

    public void deleteUser(Long id){
        userRepository.findById(id).ifPresent(user -> userRepository.delete(user));
    }

    @Override
    public UserDetailsDTO loadUserByUsername(String username) {
        User user = userRepository.findByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException(username);
        }
        return new UserDetailsDTO(mapper.toDtoBean(user));
    }

    @Override
    public UserDetailsDTO findById(Long id) {
        User user = userRepository.findById(id).orElse(null);
        return user != null ? new UserDetailsDTO(mapper.toDtoBean(user)) : null;
    }

    public ResponseSearchDTO getAllListUser(Pageable pageable){
        ResponseSearchDTO result = new ResponseSearchDTO();
        Page<User> resultSearch = userRepository.findAll(pageable);
        result.setItems(resultSearch.getContent());
        result.setTotal(resultSearch.getTotalElements());
        return result;
    }

    @Override
    public UserDTO getOne(Long id) {
        User user = userRepository.findById(id).orElse(null);
        return user != null ? mapper.toDtoBean(user) : null;
    }

    public void changePassword(String password, Long id){
        User user = userRepository.findById(id).orElse(null);
        if(user == null)
            return;
        user.setPassword(passwordEncoder.encode(password));
        userRepository.saveAndFlush(user);
    }
}
