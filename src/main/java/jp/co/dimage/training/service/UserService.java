package jp.co.dimage.training.service;

import jp.co.dimage.training.dto.ResponseSearchDTO;
import jp.co.dimage.training.dto.UserDTO;
import jp.co.dimage.training.dto.UserDetailsDTO;
import jp.co.dimage.training.exception.ValidateException;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserService extends UserDetailsService {
    UserDetailsDTO findById(Long id);
    ResponseSearchDTO getAllListUser(Pageable pageable);
    void createUser(UserDTO userDTO) throws ValidateException;
    boolean updateUser(UserDTO userDTO) throws ValidateException;
    void deleteUser(Long id);
    UserDTO getOne(Long id);
    void changePassword(String password, Long id);
}
