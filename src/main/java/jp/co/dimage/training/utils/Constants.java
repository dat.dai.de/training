package jp.co.dimage.training.utils;

public class Constants {
    private Constants(){}

    public static final String SYSTEM_ERROR = "system.error";
    public static final String SUCCESS = "success";

}
